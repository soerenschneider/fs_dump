FROM debian:10.5-slim

RUN apt update \
    && apt -y install --no-install-recommends gnupg2 awscli xz-utils \
    && apt -y clean \
    && useradd -ms /bin/bash fs_dump

COPY fs_dump /usr/bin
USER fs_dump
WORKDIR /home/fs_dump
ENTRYPOINT ["/usr/bin/fs_dump"]
